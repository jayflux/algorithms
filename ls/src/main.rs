fn main() {
	let list = vec![2,19,22,31,32,48,99,154];
	match binary_search(list, 2) {
		Some(x) => println!("Value found! {}", x),
		None => println!("No value found")
	}
}

fn binary_search(list: Vec<usize>, num: usize) -> Option<usize> {
	let mut min_index = 0;
	let mut max_index = list.len() - 1;

	while min_index <= max_index {
		let current_index = (min_index + max_index) / 2;
		let current_value = list[current_index];

		if current_value < num {
			min_index = current_index + 1;
		} else if current_value > num {
			max_index = current_index - 1;
		} else {
			return Some(current_index as usize)
		}
	}

	return None
}
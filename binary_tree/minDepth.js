function Node(key) {
	this.data = key;
	this.left = null;
	this.right = null;
}

function minDepth(root) {
	if (root === null) {
		return 0;
	}
	// We've reached a root with no leaves
	if (root.left === null && root.right === null) {
		return 1;
	}

	if (root.left === null) {
		return minDepth(root.right) + 1;
	}

	if (root.right === null) {
		return minDepth(root.left) + 1;
	}

	return Math.min(minDepth(root.left), minDepth(root.right)) + 1;
}

var root = new Node(1);
root.left = new Node(2);
root.right = new Node(3);
root.left.left = new Node(4);
root.left.right = new Node(5);
console.log(minDepth(root))
function Node(key) {
	this.key = key;
	this.left = null;
	this.right = null;
}

function treeSearch(root, x) {
	while (root !== null && root.key !== x) {
		if (x < root.key) {
			root = root.left;
		} else {
			root = root.right;
		}
	}
	return root;
}
var root = new Node(6);
root.left = new Node(5);
root.right = new Node(7);
root.left.left = new Node(2);
root.left.right = new Node(5);
root.right.right = new Node(8);
console.log(treeSearch(root, 5))
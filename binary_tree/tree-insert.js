class Tree {

	constructor() {
		this.root = null;
	}

	static createNode(key) {
		var node = {
			this.key: key,
			this.left: null,
			this.right: null
		}

		return node;
	}
	
	insert(z) {
		z = Tree.createNode(z);
		let x = this.root,
			y = null;

		while(x !== null) {
			y = x;
			if (z.key < x.key) {
				x = x.left;
			} else {
				x = x.right;
			}
		}

		z.p = y;
		if (y === null) { // the tree was empty 
			this.root = z;
		} else if (z.key < y.key) {
			y.left = z;
		} else {
			y.right = z;
		}
	}

	walk(x) {
		let x = x || this.root;
		if (x !== null) {
			this.walk(x.left);
			console.log(x);
			this.walk(x.right);
		}
	}
}



function Node(key) {
	this.key = key;
	this.left = null;
	this.right = null;
}

function treeInsert(root, z) {
	var y = null,
		x = root;

	while (x !== null) {
		y = x // trailing pointer
		if (z.key < x.key) {
			x = x.left;
		} else {
			x = x.right;
		}
	}
	// Once x hits null, we've reached the bottom of the tree, but y will be the value above
	z.p = y;
	if (y === null) {
		// the tree was empty
		root = z;
	} else if (z.key < y.key) {
		y.left = z;
	} else {
		y.right = z;
	}

}

function treeWalk(root) {
	if (root !== null) {
		treeWalk(root.left);
		treeWalk(root.right);
	}
}

// Swap subtrees
function transplant(root, u, v) {
	if (u.p == null) {
		root = v;
	} else if (u == u.p.left) {
		u.p.left = v;
	} else {
		u.p.right = v;
	}

	if (v !== null) {
		v.p = u.p;
	}
}

function treeMinimum(x) {
	while (x.left !== null) {
		x = x.left;
	}

	return x;
}


function delete(root, z) {
	if (z.left == null) {
		transplant(root, z, z.right); // swap z with its right child
	} else if (z.right == null) {
		transplant(root, z, z.left); // swap z with its left child
	} else {
		var y = treeMinimum(z.right); // fetch the successor to z
		if (y.p !== z) {
			transplant(root, y, y.right);
			y.right = z.right;
			y.right.p = y;
		}
		transplant(root, z, y);
		y.left = z.left;
		y.left.p = y;
	}
}

var root = new Node(6);
treeInsert(root, new Node(5));
treeInsert(root, new Node(7));
treeInsert(root, new Node(2));
treeInsert(root, new Node(5));
treeInsert(root, new Node(8));
treeWalk(root)

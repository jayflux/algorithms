/**
 * Definition for binary tree
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @constructor
 * @param {TreeNode} root - root of the binary search tree
 */
var BSTIterator = function (root) {
  // Keep track of the current node we're on
  this.root = root;
  this.list = [];
  // Set up the list
  this.inorderTreeWalk(root);
  this.list = this.list.reverse();
};

/**
 * @this BSTIterator
 * @returns {boolean} - whether we have a next smallest number
 */
BSTIterator.prototype.hasNext = function () {
  return !!this.list.length;
};

/**
 * @this BSTIterator
 * @returns {number} - the next smallest number
 */
BSTIterator.prototype.next = function () {
  return this.list.pop().val;
};

BSTIterator.prototype.inorderTreeWalk = function (root) {
  if (root !== null) {
    this.inorderTreeWalk(root.left);
    this.list.push(root);
    this.inorderTreeWalk(root.right);
  }
}
/**
 * Your BSTIterator will be called like this:
 * var i = new BSTIterator(root), a = [];
 * while (i.hasNext()) a.push(i.next());
*/

function Node(key) {
	this.key = key;
	this.left = null;
	this.right = null;
}

function treeMinimum(x) {
	while (x.left !== null) {
		x = x.left;
	}

	return x;
}

var root = new Node(6);
root.left = new Node(5);
root.right = new Node(7);
root.left.left = new Node(2);
root.left.right = new Node(5);
root.right.right = new Node(8);
console.log(treeMinimum(root, 5))
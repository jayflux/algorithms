class Tree {

  constructor() {
    this.root = null;
  }


  static createNode(key) {
    var node = {
      key: key,
      left: null,
      right: null
    }

    return node;
  }

  insert(z) {
    z = Tree.createNode(z);
    let x = this.root,
      y = null;

    while (x !== null) {
      y = x;
      if (z.key < x.key) {
        x = x.left;
      } else {
        x = x.right;
      }
    }

    z.p = y;
    if (y === null) { // the tree was empty
      this.root = z;
    } else if (z.key < y.key) {
      y.left = z;
    } else {
      y.right = z;
    }
  }

  walk(x) {
    let x = x || this.root;
    if (x !== null) {
      this.walk(x.left);
      console.log(x);
      this.walk(x.right);
    }
  }
}

function Node(key) {
	this.key = key;
	this.left = null;
	this.right = null;
}

function treeSuccessor(x) {
	if (x.right !== null) {
		return treeMinimum(x.right);
	}
}

function treeMinimum(x) {
	// base case
	if (x.left === null) {
		return x;
	}

	return treeMinimum(x.left);
}

var root = new Node(6);
root.left = new Node(5);
root.right = new Node(7);
root.left.left = new Node(2);
root.left.right = new Node(5);
root.right.right = new Node(8);
console.log(treeMaximum(root))
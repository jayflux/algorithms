function Node(key) {
	this.data = key;
	this.left = null;
	this.right = null;
}

function treeSearch(root, x) {
	// base case
	if (root == null || root.data == x) {
		return root;
	}

	if (x < root.data) {
		return treeSearch(root.left, x);
	} else {
		return treeSearch(root.right, x);
	}
}
var root = new Node(6);
root.left = new Node(5);
root.right = new Node(7);
root.left.left = new Node(2);
root.left.right = new Node(5);
root.right.right = new Node(8);
console.log(treeSearch(root, 5))
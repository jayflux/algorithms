function Node(key) {
	this.data = key;
	this.left = null;
	this.right = null;
}

function inorderTreeWalk(root) {
	if (root !== null) {
		inorderTreeWalk(root.left);
		console.log(root.data);
		inorderTreeWalk(root.right);
	}
}
var root = new Node(6);
root.left = new Node(5);
root.right = new Node(7);
root.left.left = new Node(2);
root.left.right = new Node(5);
root.right.right = new Node(8);
console.log(inorderTreeWalk(root))
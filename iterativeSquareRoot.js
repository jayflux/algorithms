function sqrt(num) {
	var low = 0,
		high = num + 1;

	while (high - low > 1) {
		let mid = Math.floor((low + high) / 2);
		if (mid * mid <= num) {
			low = mid;
		} else {
			high = mid;
		}
	}

	return low;
}



console.log(sqrt(18))
console.log(Math.sqrt(18))
function flatten(val, resultArr) {
	var result = resultArr || [];

	if (Array.isArray(val)) {
		for (var i = 0; i < val.length; i++) {
			flatten(val[i], result);
		}
	}
	else {
		result.push(val);
	}

	return result;
}

var jay = [1,2,3,4,[5,6,7], 8,9,10]
console.log(flatten(jay))
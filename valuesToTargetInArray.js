var arr1 = [1,2,4,4];

function find(arr, target) {
	var minIndex = 0,
		maxIndex = arr.length - 1,
		minValue = arr[minIndex],
		maxValue = arr[maxIndex];

	while(minIndex < maxIndex) {
			minValue = arr[minIndex],
			maxValue = arr[maxIndex];
		if (minValue + maxValue > target) {
			maxIndex--;
		} else if (minValue + maxValue < target) {
			minIndex++;
		} else {
			return [minIndex, maxIndex];
		}
	}

	return false;
}

console.log(find(arr1, 8))
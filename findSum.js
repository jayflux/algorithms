var arr = [10, 22, 28, 29, 30, 40];

function findSum(array, x) {
	var minIndex = 0,
		maxIndex = array.length - 1,
		diff = Infinity,
		res_l,
		res_r;

	while(minIndex < maxIndex) {
		if (Math.abs(array[minIndex] + array[maxIndex] - x) < diff) {
			diff = Math.abs(array[minIndex] + array[maxIndex] - x);
			res_l = minIndex;
			res_r = maxIndex;
		}

		if (array[minIndex] + array[maxIndex] > x) {
			maxIndex--;
		} else {
			minIndex++;
		}

	}

	console.log('sum is ' + array[res_l] + ' ' + array[res_r]);
}

findSum(arr, 54);
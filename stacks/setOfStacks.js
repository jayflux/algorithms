class SetOfStacks {
  /**
   *
   * @param {number} capacity
   */
  constructor(capacity) {
    this.stacks = [];
    this.capacity = capacity;
  }

  getLastStack() {
    if (this.stacks.length === 0) {
      return null;
    }
    return this.stacks[this.stacks.length - 1];
  }

  push(v) {
    let last = this.getLastStack();
    if (last !== null && !(last.length === this.capacity)) { // add to last stack
      last.push(v)
    } else {
      let stack = new Array();
      stack.push(v);
      this.stacks.push(stack);
    }
  }

  pop() {
    let last = this.getLastStack();
    if (last == null) throw new Error("empty stack!");
    let v = last.pop();
    if (last.length === 0) {
      this.stacks.pop();
    }
    return v;
  }
}

let setOfStacks = new SetOfStacks(4);
setOfStacks.push(1)
setOfStacks.push(2)
setOfStacks.push(3)
setOfStacks.push(4)
setOfStacks.push(5)
setOfStacks.push(6)
setOfStacks.push(7)
setOfStacks.push(8)
setOfStacks.push(9)
setOfStacks.push(10)

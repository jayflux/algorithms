function findDomNode(root, id) {
	
	var result;

	return (function findDomNodeById(root, id) {
		// set base case
		if (root.id === id) {
			result = root;
		}

		if (root.childNodes.length) {
			for (let i = 0; i < root.childNodes.length; i++) {
				findDomNodeById(root.childNodes[i], id);
			}
		}

		return result[0];
	})(root, id);

	
}


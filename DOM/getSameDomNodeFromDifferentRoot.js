// Simple indexOf abstraction
function indexOf(haystack, needle) {
  return Array.prototype.indexOf.call(haystack, needle);
}

// Starts at the target then traverses up the tree, keeping track of how far from the parent it is each time, we should end up with something like [0, 2, 5, 10] etc
function getPath(root, target) {
  // V----------------------^
  // Grab the current target
  var current = target;
  // Create an empty array for our path [stack]
  path = [];
  while (current !== root) {
    let index = indexOf(current.parentNode.childNodes, current);
    path.unshift(index);
    current = current.parentNode;
  }
  return path;
}

function locateNodeFromPath(root, path) {
  var current = root;
  // Cycle through childnodes
  for (var i = 0; i < path.length; i++) {
    current = current.childNodes[path[i]];
  }

  return current;
}

function getSameNode(root1, root2, target) {
  return locateNodeFromPath(root2, getPath(root1, target))
}

var helloDiv = document.querySelector('.hello');
var root1 = new DocumentFragment();
var root2 = new DocumentFragment();
root1.appendChild(helloDiv.cloneNode(true));
root2.appendChild(helloDiv.cloneNode(true));
window.root2 = root2;
window.root1 = root1;
var target1 = root1.querySelector('.myItem');
// console.log(target1);
getPath(root1, target1);
result = getSameNode(root1, root2, target1);
console.log(result);

class BloomFilter {
  constructor(size = 100) {
    this.size = size;
    this.storage = this.createStore(size);
  }

  createStore(size) {
    const storage = new Array(size).fill(0);
    const storageInterface = {
      getValue(index) {
        return storage[index];
      },
      setValue(index) {
        storage[index] = true;
      }
    }

    return storageInterface;
  }

  insert(item) {
    const hashValues = this.getHashValues(item);
    hashValues.forEach(val => this.storage.setValue(val));
  }

  mayContain(item) {
    const hashValues = this.getHashValues(item);

    for (let hashIndex = 0; hashIndex < hashValues.length; hashIndex++) {
      if (!this.storage.getValue(hashValues[hashIndex])) {
        // We know the item was not inserted
        return false;
      }
    }

    // item may or may not have been inserted
    return true
  }

  hash1(item) {
    let hash = 0;

    for (let charIndex = 0; charIndex < item.length; charIndex++) {
      const char = item.charCodeAt(charIndex);
      hash = (hash << 5) + hash + char;
      hash &= hash;
      hash = Math.abs(hash);
    }

    return hash % this.size;
  }


  /**
 * @param {string} item
 * @return {number}
 */
  hash2(item) {
    let hash = 5381;

    for (let charIndex = 0; charIndex < item.length; charIndex += 1) {
      const char = item.charCodeAt(charIndex);
      hash = (hash << 5) + hash + char; /* hash * 33 + c */
    }

    return Math.abs(hash % this.size);
  }

  /**
   * @param {string} item
   * @return {number}
   */
  hash3(item) {
    let hash = 0;

    for (let charIndex = 0; charIndex < item.length; charIndex += 1) {
      const char = item.charCodeAt(charIndex);
      hash = (hash << 5) - hash;
      hash += char;
      hash &= hash; // Convert to 32bit integer
    }

    return Math.abs(hash % this.size);
  }

  /**
 * Runs all 3 hash functions on the input and returns an array of results.
 *
 * @param {string} item
 * @return {number[]}
 */
  getHashValues(item) {
    return [
      this.hash1(item),
      this.hash2(item),
      this.hash3(item)
    ];
  }
}


bloomFilter = new BloomFilter(100);
bloomFilter.insert("Jason");
bloomFilter.insert("Peggy");
bloomFilter.insert("BLah1");
bloomFilter.insert("Blah2");
bloomFilter.insert("Mambo");
bloomFilter.insert("LOL");
console.log(bloomFilter.mayContain("ason"));

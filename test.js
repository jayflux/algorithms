function smallest(number) {
	let num = number.toString().split(''),
		numSorted = num.slice().sort((a, b) => a - b), // Make a copy so we keep original order
		lowestVal = numSorted[0],
		lowestValIndex = num.indexOf(numSorted[0]); // get lowest number and its index in the original array


	num.splice(lowestValIndex, 1);
	num.unshift(lowestVal);

	return [parseInt(num.join('')), lowestValIndex, 0];
}

console.log(smallest(261235));
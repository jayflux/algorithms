function deBounce(fn, duration) {
	let timer;

	return function(e) {
		clearTimeout(timer);
		timer = setTimeout(function() {
			fn(e);
		}, duration);
	} 
}
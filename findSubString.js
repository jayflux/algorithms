function findSubstring(input, alphabet) {
	var table = new Set(alphabet.split("")),
		inputLetters = input.split(""),
		subIndex,
		count = 0,
		alreadyUsed = new Set();

	inputLetters.forEach((v, i) => {
		if (table.has(v) && !alreadyUsed.has(v)) {
			count++;
			if (count === table.size) {
				subIndex = i - 2;
			}
			alreadyUsed.add(v);
		}  else {
			if (!table.has(v)) {
				count = 0;
			} else {
				count = (count > 1) ? --count : count;
			}
			alreadyUsed.clear();
		}
	});

	return subIndex;
}

console.log(findSubstring("aabbccjsdjdosjiosijba", "dos"));
# Below is the interface for Iterator, which is already defined for you.
#
# class Iterator(object):
#     def __init__(self, nums):
#         """
#         Initializes an iterator object to the beginning of a list.
#         :type nums: List[int]
#         """
#
#     def hasNext(self):
#         """
#         Returns true if the iteration has more elements.
#         :rtype: bool
#         """
#
#     def next(self):
#         """
#         Returns the next element in the iteration.
#         :rtype: int
#         """


class PeekingIterator(object):
    def __init__(self, iterator):
        """
        Initialize your data structure here.
        :type iterator: Iterator
        """
        self.iter = iterator
        self.buffer = self.iter.next()

    def peek(self):
        """
        Returns the next element in the iteration without advancing the iterator.
        :rtype: int
        """
        # Check buffer first
        if self.buffer != None:
            val = self.buffer
            self.buffer = None
            return val

    def next(self):
        """
        :rtype: int
        """
        # We should check buffer first
        if self.buffer != None:
            val = self.buffer
            self.buffer = None
        else:
            val = self.iter.next()
            if self.iter.hasNext():
                self.buffer = self.iter.next()
        print val
        return val

    def hasNext(self):
        """
        :rtype: bool
        """
        return self.buffer or self.iter.hasNext()


# Your PeekingIterator object will be instantiated and called as such:
# iter = PeekingIterator(Iterator(nums))
# while iter.hasNext():
#     val = iter.peek()   # Get the next element but not advance the iterator.
#     iter.next()         # Should return the same value as [val].

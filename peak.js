let arr = [1, 10, 5, 18, 21, 5];

function findPeakUtil(arr, min, max) {
  // Get the middle element
  let mid = Math.floor((min + max) / 2);

  // Compare middle element with its
  // neighbours (if neighbous exists)
  if ((mid === 0 || arr[mid - 1] <= arr[mid]) && (mid === arr.length - 1 || arr[mid + 1] <= arr[mid])) {
    return arr[mid];
  }
  else if (mid > 0 && arr[mid - 1] > arr[mid]) {
    return findPeakUtil(arr, min, mid - 1);
  }

  else {
    return findPeakUtil(arr, mid + 1, max)
  }
}

console.log(findPeakUtil(arr, 0, arr.length - 1))

// Write an emitter class
/*
emitter = new Emitter();

// 1. Support subscribing to events.
sub = emitter.subscribe('event_name', callback);
sub2 = emitter.subscribe('event_name', callback2);

// 2. Support emitting events.
// This particular example should lead to the `callback` above being invoked with `foo` and `bar` as parameters.
emitter.emit('event_name', foo, bar);

// 3. Support unsubscribing existing subscriptions by releasing them.
sub.release(); // `sub` is the reference returned by `subscribe` above
*/

function EventEmitter() {
  this.events = {};
}

EventEmitter.prototype.subscribe = function (evt, callback) {
  var that = this;
  if (evt in this.events) {
    this.events[evt].push(callback);
  } else {
    this.events[evt] = [callback];
  }

  var sub = {
    release: () => {
      // let idx = this.events[evt].indexOf(callback);
      // this.events[evt].splice(idx, 1);
      this.events = this.events[evt].filter(v => v != callback);
    }
  }

  return sub;
}

EventEmitter.prototype.emit = function (evt) {
  var args = Array.prototype.slice(arguments, 1); // we don't care about the event name
  if (evt in this.events) {
    this.events[evt].forEach((v, i) => {
      v(args);
    });
  }
}

let emitter = new EventEmitter();
let sub = emitter.subscribe('test', () => console.log('testing'));
emitter.emit('test');
sub.release()

#![allow(dead_code)]
#![allow(unused_variables)]
use std;

pub fn binary_search<T: std::cmp::PartialOrd>(list: Vec<T>, needle: T) -> Option<usize> {
	let mut min_index = 0;
	let mut max_index = list.len() - 1;
	let mut current_index: usize;
	let mut current_value: &T;

	while min_index < max_index {
		current_index = (min_index + max_index) / 2;
		current_value = list.get(current_index).unwrap();

		if current_value < &needle {
			min_index = current_index + 1;
		}

		else if current_value > &needle {
			max_index = current_index - 1;
		}

		else {
			return Some(current_index)
		}
	}


	None
}
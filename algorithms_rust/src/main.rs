extern crate algorithms;

use algorithms::binary_trees::binary_search::binary_search;

fn main() {
	// let list: Vec<usize> = vec![2, 5, 6, 7, 8, 9, 10, 14, 18, 19, 20];
	let list: Vec<&'static str> = vec!["a", "b", "c", "d", "e", "f"];
	let val: usize = binary_search(list, "c").unwrap();
	println!("{}", val);
}
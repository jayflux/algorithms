#![allow(dead_code)]
use std;
fn bubble_sort<T: std::cmp::PartialOrd>(list: &mut Vec<T>) {
	let length = list.len();

	for _ in 0..length {
		for j in 0..length - 1 {
			if list[j] > list[j + 1] {
				// swap
				list.swap(j, j + 1);
			}
		}
	}
}
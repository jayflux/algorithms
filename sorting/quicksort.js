var arr = [1,3,5,2,80,3,71,12];

function sort(array) {

  // Establish a base case
  if (array.length <= 1) return array;

  let swapPos = Math.floor((array.length - 1) / 2),
    swapVal = array[swapPos],
    less  = [],
    more  = [];

  array = array.slice(0, swapPos).concat(array.slice(swapPos + 1));
  array.forEach((v, i, array) => {
    if (array[i] < swapVal) {
      less.push(array[i]);
    } else {
      more.push(array[i]);
    }
  });

  return sort(less).concat([swapVal], sort(more));


}

console.log(sort(arr))
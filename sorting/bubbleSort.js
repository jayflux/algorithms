var a = [34, 203, 3, 746, 200, 984, 198, 764, 9];

function bubbleSort(a) {
  for (var i = a.length - 1; i > 1; i--) {
    for (var j = 0; j < i; j++) {
      if (a[j] > a[j + 1]) {
        // swap
        var temp = a[j];
        a[j] = a[j + 1];
        a[j + 1] = temp;
      }
    }
  }

  return a;
}

console.log(bubbleSort(a));

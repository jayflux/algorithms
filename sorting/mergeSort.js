function sort(arr) {
	var length 	= arr.length,
		mid 	= Math.floor(length * 0.5),
		left 	= arr.slice(0, mid), // mid is inclusive, so won't actually include mid
		right	= arr.slice(mid, length);

	if (length === 1 || length === 0) {
		return arr;
	}

	//          sorted              sorted
	//             ^                  ^
    //             |                  |
	return merge(sort(left), sort(right));
}


function merge(left, right) {
	var result = [];

	while(left.length || right.length) {

		if(left.length && right.length) {
			if (left[0] < right[0]) {
				result.push(left.shift());
			} else {
				result.push(right.shift());
			}
		}
		else if (left.length) {
			result.push(left.shift());
		} else {
			result.push(right.shift());
		}



	}

	return  result;
}

var arr = [1,3,5,2,80,3,71,12]
console.log(sort(arr)) 
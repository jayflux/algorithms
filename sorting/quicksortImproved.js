function quicksort(arr, left, right) {
  // Move the values to the right and left of the partition and return it
  let index = partition(arr, left, right);
  if (left < index - 1) { // sort left half
    quicksort(arr, left, index - 1);
  }
  if (index < right) {
    quicksort(arr, index, right);
  }
}


function partition(arr, left, right) {
  let pivotIndex = Math.floor((left + right) / 2);
  let pivot = arr[pivotIndex];

  while (left <= right) {
    // Find element on the left that should be on the right
    while (arr[left] < pivot) left++;
    // Find element on the right that should be on the left
    while (arr[right] > pivot) right--;

    if (left <= right) {
      // swap elements
      let tmp = arr[left];
      arr[left] = arr[right];
      arr[right] = tmp;
      left++;
      right--
    }
  }

  return left;
}

let a = [9, 3, 2, 1, 4, 5, 3, 2];
quicksort(a, 0, a.length - 1);
console.log(a)

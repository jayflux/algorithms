var arr = [7, 90, 130, 4, 33, 600];

function findMax(arr) {
	var maxDiff = -Infinity,
		pairs = {};
	for (var i = 0; i < arr.length; i++) {
		for (var j = i+1; j < arr.length; j++) {
			if ((arr[i] < arr[j]) && Math.abs(arr[j] - arr[i]) > maxDiff) {
				maxDiff = Math.abs(arr[j] - arr[i]);
				pairs['a'] = arr[i];
				pairs['b'] = arr[j];
			}
		}
	}

	return pairs;
} 

console.log(findMax(arr));
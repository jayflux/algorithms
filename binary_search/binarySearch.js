arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// the array needs to be sorted for a binary Search to work
var sorted = arr.sort();

// Return the index of the search term
function binarySearch(num) {
	var minIndex = 0,
		maxIndex = this.length - 1,
		currentIndex,
		currentValue;
		// Should always get us to the middle of the array
		
	while (minIndex <= maxIndex) {
		currentIndex = Math.floor((minIndex + maxIndex) / 2);
		
		currentValue = this[currentIndex]; // using Prototype (call or Apply);

		if (currentValue < num) {
			minIndex = currentIndex + 1;
		} 
		else if (currentValue > num) {
			maxIndex = currentIndex - 1;
		}
		else {
			console.log('arr[' + currentIndex + '] = ' + currentValue);
			return currentIndex;
		}
	}

	return -1;
}

binarySearch.call(sorted, 9);
arr = [];
for (var i = 0; i < 20; i++) {
 	arr.push(Math.floor(Math.random(100) *  10));
}

// the array needs to be sorted for a binary Search to work
var sorted = arr.sort();
console.log(sorted);

// Return the index of the search term

function binarySearch(array, num) {

	return (function innerBinarySearch(array, num, minIndex, maxIndex) {
		if (minIndex > maxIndex) return -1;
		currentIndex = (minIndex + maxIndex) / 2 | 0;
		currentValue = array[currentIndex];

		if (currentValue < num) {
			return innerBinarySearch(array, num, currentIndex + 1, maxIndex);
		} else if (currentValue > num) {
			return innerBinarySearch(array, num, minIndex, currentIndex - 1);
		} else {
			return currentIndex;
		}
	})(array, num, 0, array.length);
}

console.log(binarySearch(sorted, 4));
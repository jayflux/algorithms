arr = [1,3,20,4,1,0,7];

function findPeakUtil(arr, low, high) {
	// Find index of midle element
	let mid = low + (high - low) / 2 | 0;
	let len = arr.length

	// compare middle element with its neighbours (if neighbours exist)
	if ((mid == 0 || arr[mid - 1] <= arr[mid]) && 
		(mid == len-1 || arr[mid+1] <= arr[mid]))  {
			return mid;
	}

	// if middle element is not peak and its left neighber is greater than it
	// then the left half must have a peak element
	else if (mid > 0 && arr[mid - 1] > arr[mid])
		return findPeakUtil(arr, low, mid-1);

	// if middle element is not preak and its right neighbour is greater than it
	// then right half must have a peak
	else {
		return findPeakUtil(arr, mid + 1, high);
	}
}

function findPeak(arr) {
	return findPeakUtil(arr, 0, arr.length - 1);
}

console.log(findPeak(arr))
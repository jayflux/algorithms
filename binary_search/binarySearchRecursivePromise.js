arr = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
// the array needs to be sorted for a binary Search to work
var sorted = arr.sort();
console.log(sorted);

function binarySearch(arr, num) {
	return (function bsInner(arr, num, minIndex, maxIndex) {
		let promise = new Promise((res, rej) => {
			setTimeout(() => {
				if (minIndex >= maxIndex) res(-1);
				let currentIndex = (minIndex + maxIndex) / 2 | 0;
				let currentValue = arr[currentIndex];

				if (num > currentValue) {
					res(bsInner(arr, num, currentIndex + 1, maxIndex));
				} else if (num < currentValue) {
					res(bsInner(arr, num, minIndex, currentIndex - 1));
				} else {
					res(currentIndex)
				}
			}, 0)
		})

		return promise;
	})(arr, num, 0, arr.length - 1)
}

binarySearch(sorted, 2).then((v) => {
	console.log(v)
}, (e) => {
	console.log(e)
});
let arr = [-2,1,-3,4,-1,2,1,-5,4];

// Kadanes algorithms, looks for all the positive contigious segments along the array
function maxSubArr(nums) {
	let maxSoFar = nums[0];
	let currMax = nums[0];
	 for (let i = 1; i < nums.length; i++) {
	 	currMax = Math.max(nums[i], currMax + nums[i]);
	 	maxSoFar = Math.max(maxSoFar, currMax);
	 }

	 return maxSoFar;

}

console.log(maxSubArr(arr));
function LinkedList() {
	this.head = {
		data: null,
		next: null
	};
}

LinkedList.prototype.add = function(data) {
	var node = {
		data: data,
		next: null
	}

	if (this.head.next) {
		var current = this.head;
		while (current) {
			if (current.next === null) {
				current.next = node;
				break;
			}

			current = current.next;
		}
	} else {
		this.head.next = node;
	}

	return node;
}

LinkedList.prototype.remove = function(data) {
	var current = this.head;

	// First we start with the head
	if (current.data === data) {
		this.head.next = current.next;
	}

	else {
		var previous = current;

		// This will only run till the last-but-one
		while (current.next) {
			if (current.data === data) {
				previous.next = current.next;
				break;
			}


			previous = current;
			current = current.next;

			
		}

		// Now we're on the tail
		if (current.data === data && current.next === null) {
			previous.next = null;
		}
	}
}

LinkedList.prototype.find = function(data) {
	var current = this.head;
	while (current) {
		if (current.data === data) {
			return current;
		}

		current = current.next;
	}

	return null;
}

LinkedList.deleteDuplicates = function(n) {
	var table = {},
		previous = null;

	while (n !== null) {
		if (table[n.data]) {
			previous.next = n.next;
		} else {
			table[n.data] = true;
			previous = n;
		}
		n = n.next;
	}
}

linkedList = new LinkedList();
linkedList.add(1);
linkedList.add(1);
linkedList.add(1);
linkedList.add(1);
linkedList.add(2);
linkedList.add(3);
linkedList.remove(2)
LinkedList.deleteDuplicates(linkedList.head);
console.log(linkedList.head);
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function(height) {
	let ans = 0;
	let maxLeft = [];
	let maxRight = [];
	// Get the left max, start by matching the first
	maxLeft[0] = height[0];
	for (let i = 1; i < height.length; i++) {
		maxLeft[i] = Math.max(height[i], maxLeft[i - 1]);
	}

	// Get the right max, start by matching the end
	maxRight[height.length - 1] = height[height.length - 1];
	for (let i = height.length - 2; i >= 0; i--) {
		maxRight[i] = Math.max(height[i], maxRight[i + 1]);
	}

	for (let i =  1; i < height.length - 1; i++ ) {
		ans += Math.min(maxLeft[i], maxRight[i]) - height[i];
	}

	return ans;
};

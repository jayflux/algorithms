/**
 * @param {string} s
 * @param {number} k
 * @return {number}
 */
var lengthOfLongestSubstringKDistinct = function(s, k) {
	s = s.split('');
	let collection = new Set();
	let maxSubStr = 0;
	// Lets loop through
	for(let i = 0; i < s.length; i++) {
		let subStrLen = 0;
		for (let j = i; j < s.length; j++) {
			if (collection.has(s[j])) {
				subStrLen++;
				continue;
			} else if (collection.size < k) {
				collection.add(s[j]);
				subStrLen++;
			} else {
				break;
			}
		}
		maxSubStr = Math.max(subStrLen, maxSubStr);
		collection.clear()
	}

	return maxSubStr;
};

console.log(lengthOfLongestSubstringKDistinct("bacc", 2));
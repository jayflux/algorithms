/**
 * @param {number[]} digits
 * @return {number[]}
 * https://www.youtube.com/watch?v=H_NajpLW-hU
 * 
 * 19 becomes more interesting because you increment the 1 and the 9 becomes a 0
 * There's another case because what if you have a [9] or a [9, 9], the [9, 9] would become a [1,0,0] 
 * So we would need to add a new element
 */
var plusOne = function(digits) {
	for (let i = digits.length - 1; i >= 0; i--) {
		if (digits[i] < 9) {
			digits[i] += 1;
			// At this point we don't need to go any further
			return digits;
		}
		digits[i] = 0;
	}
	// Then the final condition is if you see all 9s
	// Then we want to increase the elements in our list
	let number = new Array(digits.length + 1);
	number.fill(0);
	number[0] = 1;
	return number;
};

console.log(plusOne([6,1,4,5,3,9,0,1,9,5,1,8,6,7,0,5,5,4,3]));
/**
 * @param {number[]} nums
 * @return {number}
 * https://leetcode.com/explore/interview/card/google/59/array-and-strings/455/discuss/96923/Nice-java-solution-and-easy-to-understand
 * O(n)
 * space O(1)
 */
var findMaxConsecutiveOnes = function(nums) {
    let max = 0;
    let len = 0; // Size of length of 1s
    let lenFlip = 0; // length of 1s with 1 flip
    // This lenFLip works well because if we hit our first 0 lenFlip will keep on going, on our second zero lenFLip will be less than max
    nums.forEach(v => {
        if (v ==1) {
            len++;
            lenFlip++;
        } else {
            lenFlip = len + 1; // the order here is important, we need lenFlip defined before we reset len
            len = 0;
        }
        max = Math.max(max, lenFlip);
    });

    return max;
};
/**
 * @param {number[]} nums
 * @return {number}
 * https://www.geeksforgeeks.org/maximum-consecutive-ones-or-zeros-in-a-binary-array/
 * O(n)
 * space O(1)
 */
var findMaxConsecutiveOnes = function(nums) {
    let count = 0;
    let result = 0;
    nums.forEach(v => {
    	if (v === 0) {
    		count = 0;
    	} else {
    		count += 1;
    		result = Math.max(result, count);
    	}
    });

    return result;
};
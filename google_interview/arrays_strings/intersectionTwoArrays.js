/**
 * @param {number[]} nums1
 * @param {number[]} nums2
 * @return {number[]}
 */
var intersection = function(nums1, nums2) {
    if (nums1.length > nums2.length) {
    	return getInterUtil(nums1, nums2);
    }

    return getInterUtil(nums2, nums1);
};

function getInterUtil(s, l) {
	let set = new Set();
	l.forEach((v, i) => {
		s.forEach(j => {
			if (v === j) {
				set.add(v);
			}		});
	});
	return Array.from(set);
}

console.log(intersection([1, 2, 2, 1], [2, 2]));
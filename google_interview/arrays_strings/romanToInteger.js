/**
 * @param {string} s
 * @return {number}
 */
var romanToInt = function (s) {
  // initialize result
  let res = 0;

  for (let i = 0; i < s.length; i++) {
    let s1 = value(s[i]);
    if (i + 1 < s.length) {
      // Get value of symbol s[i+1]
      let s2 = value(s[i + 1]);

      // Compare both values
      if (s1 >= s2) {
        // Value of current symbol is greater or equal to next symbol
        res = res + s1;
      } else {
        // value of current symbol is less
        res = res + s2 - s1;
        i++;
      }
    } else {
      res = res + s1;
      i++;
    }
  }

  return res;
};

// Return the numeric value of numeral
var value = function (r) {
  if (r == 'I')
    return 1;
  if (r == 'V')
    return 5;
  if (r == 'X')
    return 10;
  if (r == 'L')
    return 50;
  if (r == 'C')
    return 100;
  if (r == 'D')
    return 500;
  if (r == 'M')
    return 1000;

  return -1;
}

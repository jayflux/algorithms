/**
 * @param {number[][]} board
 * @return {void} Do not return anything, modify board in-place instead.
 * @video https://www.youtube.com/watch?v=tENSCEO-LEc
 * @video https://www.youtube.com/watch?v=FWSR_7kZuYg
 * @desc https://leetcode.com/problems/game-of-life/discuss/73218/JavaScript-in-place-Solution
 * https://leetcode.com/problems/game-of-life/discuss/73271/Python-Solution-with-detailed-explanation
 */
var gameOfLife = function(board) {
  // Iterate the board
  for (let i = 0; i < board.length; i++) {
    // iterate the rows
    for (let j = 0; j < board[0].length; j++) {
      let liveCells = aliveNeighbors(board, i, j);
      if (board[i][j] === 1 && (liveCells < 2 || liveCells > 3)) {
        board[i][j] = 2; // dead
      }
      if (board[i][j] === 0 && liveCells === 3) {
        board[i][j] = 3; // alive
      }
    }
  }

  for (let i = 0; i < board.length; i++) {
    for (let j =0; j < board[0].length; j++) {
      // 3 would return 1
      // 2 would return 0
      //  Modulo then equals
      board[i][j] %= 2
    }
  }
};


let aliveNeighbors = (board, i, j) => {
  let count = 0;
  // We don't include the current item, so we miss out [0, 0]
  let indexes = [[1, -1], [1, 0], [1, 1], [0, -1], [0, 1], [-1, -1], [-1, 0], [-1, 1]];
  for (let index of indexes) {
    // block out edge cases
    if (index[0] + i < 0 || index[0] + i > board.length - 1 ||
      index[1] + j < 0 || index[1] + j > board[0].length - 1) continue;

    if (board[index[0] + i][index[1] + j] === 1 || board[index[0] + i][index[1] + j] === 2) 
      count++;
  }

  return count
}

gameOfLife([[1]])
/**
 * @param {number[]} height
 * @return {number}
 */
var trap = function(height) {
	let ans = 0;
	for (let i = 1; i < height.length; i++) {
		let maxLeft = 0, maxRight = 0;
		// at the least O(n)
		for (let j = i; j >= 0; j--) { // Search the left part
			maxLeft = Math.max(maxLeft, height[j]);
		}

		for (let j = i; j <= height.length - 1; j++) {
			maxRight = Math.max(maxRight, height[j]);
		}

		// We want the smallest boundry, then we need to remove the height
		// so we're left with the square the water covers
		ans += Math.min(maxLeft, maxRight) - height[i];
	}

	return ans;
};

console.log(trap([0,1,0,2,1,0,1,3,2,1,2,1]));
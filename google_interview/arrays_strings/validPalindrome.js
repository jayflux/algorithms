/**
 * @param {string} s
 * @return {boolean}
 */
var isPalindrome = function (s) {
  if (s && s.trim()) {
    let i = 0, j = s.length - 1;
    while (j > i) {
      // This is just trimming the ends of the strings (discounting whitespace)
      while (s[i].search(/[a-zA-Z0-9]/) === -1) i++;
      while (s[j].search(/[a-zA-Z0-9]/) === -1) j--;

      if (s[i].toLowerCase() === s[j].toLowerCase()) {
        i++;
        j--;
      } else {
        return false;
      }
    }

    return true;
  }

  return false;
};

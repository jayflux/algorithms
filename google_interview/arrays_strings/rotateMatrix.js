/**
 *
 * @param {number[][]} matrix
 * @return {boolean}
 */
function rotate(matrix) {
  // First lets return false if the matrix isn't valid
  if (matrix.length === 0 || matrix[0].length !== matrix.length) return false;

  let n = matrix.length;
  for (let layer = 0; layer < n / 2; layer++) {
    let first = layer;
    let last = n - 1 - layer;
    for (let i = first; i < last; i++) {
      let offset = i - first;
      let top = matrix[first][i]; // save top

      // left -> top
      matrix[first][i] = matrix[last - offset][first]

      // bottom -> left
      matrix[last - offset][first] = matrix[last][last - offset]

      // right -> bottom
      matrix[last][last - offset] = matrix[i][last];

      // top -> right
      matrix[i][last] = top;
    }
  }

  console.log(matrix);
  return true;
}

// Driver program
let m = [
  [
    1,2,3,4
  ],
  [
    5,6,7,8
  ],
  [
    9,10,11,12
  ],
  [
    13,14,15,16
  ]
]
rotate(m)

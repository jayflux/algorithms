/**
 * @param {number[]} nums
 * @return {number}
 */
var firstMissingPositive = function(nums) {
	debugger;
  let i = 0;
  let p = nums.length;

  while(i < p) {
  	let n = nums[i];
  	if (n == i+1) 
  		++i;
  	else if (n < 1 || n > p || n == nums[n - 1])
  		nums[i] = nums[--p]; // useless. get new candidate
  	else {
  		nums[i] = nums[n - 1]; // within range; put it in the right place
  		nums[n - 1] = n;
  	}
  }

  return p + 1;

};

console.log(firstMissingPositive([1,3,4]));
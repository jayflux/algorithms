/**
 * @param {string} s
 * @return {string}
 * https://leetcode.com/problems/shortest-palindrome/discuss/60281/Simple-Javascript-O(n2)-solution-140ms
 */
var shortestPalindrome = function(s) {
	debugger;
    let prefix = "";
    let pos, head, tail;

    for (pos = head = tail = parseInt(s.length / 2); pos > 0; head = tail = --pos) {
    	// move head pointer in right place if even characters
    	while (head !== 0 && s[head - 1] == s[head]) {
    		head--; pos--;
    	}
    	// move tail pointer in right place if palindrome has even characters
    	while (tail !== s.length - 1 && s[tail + 1] === s[tail]) {
    		tail++;
    	}
    	let isSame = true;
    	while(head >= 0) {
    		if (s[head] !== s[tail]) {
    			isSame = false;
    			break;
    		}
    		head--; tail++;
    	}
    	if (isSame) {
    		break;
    	}
    	console.log(pos)
    }

    for (let k = s.length - 1;k >= tail && k !== 0; k--) {
    	prefix += s[k];
    }

    return prefix + s;
};

console.log(shortestPalindrome("HANNAJ"))
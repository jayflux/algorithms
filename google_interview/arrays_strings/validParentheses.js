/**
 * @param {string} s
 * @return {boolean}
 * https://leetcode.com/explore/interview/card/google/59/array-and-strings/467/
 * We can treat this one like a compiler, and use a stack to keep track of the scope we're in
 * O(n)
 */
var isValid = function(s) {
    let stack = [];

    for (let i=0; i < s.length; i++) {
    	let cur = s.charAt(i);
    	// If we sart with a parenthesis, increase the stack
    	if (cur == '(' || cur == '{' || cur == '[') {
    		stack.push(cur);
    	}
    	else if (cur == ')') {
    		let next = stack.pop();
    		if (next !== '(') {
    			return false;
    		}
    	}
    	else if (cur == ']') {
    		let next = stack.pop();
    		if (next !== '[') {
    			return false;
    		}
    	}
    	else if (cur == '}') {
    		let next = stack.pop();
    		if (next !== '{') {
    			return false;
    		}
    	}
    }
    if (stack.length !== 0) {
    	return false;
    }
    return true;
};
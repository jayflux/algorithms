/**
 * https://www.youtube.com/watch?v=TmweBVEL0I0
 * @param {number[][]} matrix
 * @return {number[]}
 */
var spiralOrder = function(matrix) {
	// We could be given an empty array
	if (!matrix.length) return [];
    // Lets get the last row and the last column
    let lastRow = matrix.length - 1;
    let lastCol = matrix[0].length - 1;

    // We need somewhere to store our results
    let result = [];
    
    // r for row, c for column, both start at 0;
    let r = c = 0;

    while(r <= lastRow && c <= lastCol) {
    	// First process the first row..
    	for (let i = c; i <= lastCol; i++) {
    		result.push(matrix[r][i]);
    	}
    	// Now we've finished with this row, increment
    	r++;

    	// Now walk down the far right column
    	for (let i = r; i <= lastRow; i++) {
    		result.push(matrix[i][lastCol]);
    	}
    	lastCol--

    	// Now walk to the left, back to c
    	if (r <= lastRow) {
    		for (let i = lastCol; i >= c; i--) {
    			result.push(matrix[lastRow][i]);
    		}
    		lastRow--
    	}
    	if (c <= lastCol) {
    		for (let i = lastRow; i >= r; i--) {
    			result.push(matrix[i][c]);
    		}
    		c++
    	}

    }

    return result;
};

let matr = [
	['a', 'b', 'c', 'd', 'e'],
	['f', 'g', 'h', 'i', 'j'],
	['u', 'v', 'm', 'n', 'o'],
	['p', 'q', 'r', 's', 't']
]

console.log(spiralOrder(matr));
/**
 * Definition for read4()
 * 
 * @param {character[]} buf Destination buffer
 * @return {number} The number of characters read
 * read4 = function(buf) {
 *     ...
 * };
 */

/**
 * @param {function} read4()
 * @return {function}
 */
var solution = function(read4) {
    /**
     * @param {character[]} buf Destination buffer
     * @param {number} n Maximum number of characters to read
     * @return {number} The number of characters read
     */
    return function(buf, n) {
        let r = 4;
        let sum = 0;
        while(sum < n && r == 4) {
            // create a temp buffer
            let temp = [];
            // read 4 characters into the temp buffer.
            r = read4(temp);
            //this following line will handle both cases that when the number of chars in the file is less than n, or n is not the multiple of 4.
            let min = Math.min(n - sum, r);
            for (let i = 0; i < min; i++) {
                buf[sum+i] = temp[i];
            }
            sum += min;
        }
        return sum;
        
    };
};
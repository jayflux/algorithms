function setZeros(matrix) {
  let rowB = 0;
  let columnB = 0;

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if (matrix[i][j] === 0) {
        rowB |= (1 << i);
        columnB |= (1 << j);
      }
    }
  }

  // nullify rows
  for (let i = 0; i < matrix.length; i++) {
    if (getBit(rowB, i)) nullifyRow(matrix, i);
  }

  // nullify columns
  for (let j = 0; j < matrix[0].length; j++) {
    if (getBit(columnB, j)) nullifyColumn(matrix, j);
  }
}

function getBit(vector, index) {
  return (vector >> index) & 1;
}

function nullifyRow(matrix, row) {
  for (let j = 0; j < matrix[0].length; j++) {
    matrix[row][j] = 0;
  }
}

function nullifyColumn(matrix, column) {
  for (let i = 0; i < matrix.length; i++) {
    matrix[i][column] = 0;
  }
}




let foo = [
  [1, 2, 3, 4, 5],
  [6, 0, 8, 9, 10],
  [11, 12, 13, 14, 15]
]

setZeros(foo);
console.log(foo)

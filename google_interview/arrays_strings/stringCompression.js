function stringCompress(str) {
  // We should create an object which counts the letters then print them out
  let charCount = {};
  let newString = "";
  for (let i = 0; i < str.length; i++) {
    charCount[str[i]] = (charCount[str[i]]) ? ++charCount[str[i]] : 1;
  }

  for (i in charCount) {
    newString += i + charCount[i];
  }

  return newString > str ? str : newString;
}

console.log(stringCompress("aaaaaaaaaaaabbcc"))

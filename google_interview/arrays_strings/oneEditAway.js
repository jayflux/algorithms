function oneEditAway(first, second) {
  if (first.length === second.length) {
    return oneEditReplace(first, second); // At this point we know that they're the same length so only a letter may need changing
  } else if (first.length + 1 == second.length) {
    return oneEditInsert(first, second);
  } else if (first.length - 1 == second.length) {
    return oneEditInsert(second, first);
  }

  return false;
}

function oneEditReplace(s1, s2) {
  // This is our flag, to see if a difference has been set
  // The second time we return false
  let foundDifference = false;
  for (let i = 0; i < s1.length; i++) {
    if (s1[i] !== s2[i]) {
      if (foundDifference) {
        return false;
      }
      foundDifference = true;
    }
  }
  return true;
}

// The first argument will always be the shortest string
function oneEditInsert(s1, s2) {
  let index1 = 0;
  let index2 = 0;
  while (index1 < s1.length && index2 < s2.length) {
    if (s1.charAt(index1) !== s2.charAt(index2)) {
      if (index1 !== index2) {
        return false;
      }
      index2++;
    } else {
      index1++;
      index2++;
    }
  }

  return true;
}

console.log(oneEditAway('jase', 'ase'));

// https://leetcode.com/problems/first-missing-positive/discuss/17298/Sharing-my-2ms-C-solution 
var firstUniqChar = function(s) {
   for(i=0;i<s.length;i++){
       if (s.indexOf(s[i])===s.lastIndexOf(s[i])){
          return i;
      } 
   }
   return -1;
};


/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 * Time O(n)
 * Space O(1)
 * https://www.geeksforgeeks.org/check-if-two-given-strings-are-at-edit-distance-one/
 */
var isOneEditDistance = function(s, t) {
	// Find lengths of given strings
    let m = s.length, n = t.length;

    // If difference between lengths is more than 1, then strings can't be at one distance
    if (Math.abs(m - n) > 1) {
    	return false;
    }

    // our count of edits
    let count = 0; 

    // we use these values to help us cycle through the characters
    let i = 0, j = 0;
    while (i < m && j < n) { // O(n) - we need to iterate through every letter
    	// if current characters don't match
    	if (s[i] != t[i]) {
    		// If current characters don't match and our count is already 1 we can't one distance edit
    		if (count == 1) {
    			return false;
    		}

    		// if length of one string is more,
    		// then only possible edit is to remove a character
    		if (m > n) {
    			i++;
    		} else if (m < n) {
    			j++;
    		} else { // lengths of both strings are the same, move on
    			i++;
    			j++;
    		}
    		// increment count of edits
    		count++
    	}

    	else {
    		i++;
    		j++;
    	}
    }
    // if last character is extra in any string
    if (i < m || j < n) {
    	count++;
    }

    return count === 1;
};
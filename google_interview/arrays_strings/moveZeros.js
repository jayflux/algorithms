/**
 * @param {number[]} nums
 * @return {void} Do not return anything, modify nums in-place instead.
 */
var moveZeroes = function(nums) {
	let j = 0; // j is our count of how many (non-0) numbers we have, we then use
	// the remaining of that (line 14, between j and length) to fill with 0s
	// move all the nonzero elements advance
	nums.forEach((v, i) => {
		if (nums[i] != 0) {
			nums[j++] = nums[i];
		}
	})
	for (;j < nums.length; j++) {
		nums[j] = 0;
	}

	return nums
};

console.log(moveZeroes([0,0,1]));
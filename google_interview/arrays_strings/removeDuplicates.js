/**
 * @param {number[]} nums
 * @return {number}
 */
var removeDuplicates = function(nums) {
	let j = 0;
	let set = new Set();

	for(let i = 0; i < nums.length; i++) {
		if (!set.has(nums[i])) {
			set.add(nums[i]) // O(1)
			nums[j++] = nums[i];
		}
	}

	return nums.slice(0, j)
};

console.log(removeDuplicates([1,1,2]))
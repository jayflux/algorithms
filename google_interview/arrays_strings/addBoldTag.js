/**
 * @param {string} s
 * @param {string[]} dict
 * @return {string}
 * http://massivealgorithms.blogspot.co.uk/2017/06/leetcode-616-add-bold-tag-in-string.html
 */
var addBoldTag = function(s, dict) {
    // A True value at bold[i] indicates that the current character is a part of the substring which is present in dict
    let bold = new Array(s.length).fill(false);
    for (d of dict) {
        // Only need to iterate *up to* the substring, so no need to include the length of it
        // Its basically an easier way of breaking early
        for (let i = 0; i <= s.length - d.length; i++) {
            if (s.substring(i, i + d.length) === d) {
                for (let j = i; j < i + d.length; j++) {
                    bold[j] = true;
                }
            }
        }
    }

    let res = "";
    for (let i = 0; i < s.length;) {
        if (bold[i]) {
            res = res.concat("<b>");
            while(i < s.length && bold[i]) {
                res = res.concat(s.charAt(i++));
            }
            res = res.concat("</b>");
        } else {
            res = res.concat(s.charAt(i++));
        }
    }
    return res;
};



console.log(addBoldTag("abcdef", ["a","b","c","d","e","f","g"]))
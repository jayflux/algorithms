function oneEditAway(first, second) {
  // No point carrying on if the difference is more than one
  if (Math.abs(first.length < second.length) > 1) {
    return false
  }

  // Get the shorter and longer string;
  let s1 = first.length < second.length ? first : second;
  let s2 = first.length < second.length ? second : first;

  let index1 = 0;
  let index2 = 0;
  let foundDifference = false;

  while (index2 < s2.length && index1 < s1.length) {
    if (s1.charAt(index1) !== s2.charAt(index2)) {
      // Check if this is the first difference found
      if (foundDifference) return false;
      foundDifference = true;

      // on replace, move the shorter pointer
      if (s1.length === s2.length) {
        index1++;
      }
    } else {
      index1++;
    }
    index2++;
  }

  return true;
}

console.log(oneEditAway('jase', 'ase'));

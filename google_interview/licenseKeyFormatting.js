/**
 * @param {string} S
 * @param {number} K
 * @return {string}
 */
var licenseKeyFormatting = function(S, K) {
    S = S.toUpperCase().replace(/-/g, '').split('');
	const remaining = S.length % K;
	const result = [];
	if (remaining ) {
		result.push(...S.splice(0, remaining));
		result.push('-');
	}
	while(S.length) {
		result.push(...S.splice(0, K));
		result.push('-')
	}
	result.pop(); // remove last -
	return result.join('');
};

console.log(licenseKeyFormatting("2-4A0r7-4k", 3))
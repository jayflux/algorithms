/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

 /**
 * If edges are n
 * nodes are n plus 1
 * # Time: O(n)
 * # Space: O(n)
 https://leetcode.com/problems/longest-univalue-path/discuss/108142/Python-Simple-to-Understand
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
var longestUnivaluePath = function(root) {
	let longestPath = 0;
	function longest(root) {
		// base case
		if (!root) {
			return 0
		}
		let leftP = longest(root.left);
		let rightP = longest(root.right);
		let left = (root.left && root.val === root.left.val) ? (leftp + 1) : 0;
		let right = (root.right && root.val === root.right.val) ? (rightp + 1) : 0;
		longestPath = Math.max(longestPath, left + right);
		return Math.max(left, right);
	}
	longest(root)
	return longestPath;
};
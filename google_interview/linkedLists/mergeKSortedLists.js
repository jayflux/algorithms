/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode[]} lists
 * @return {ListNode}
 * https://leetcode.com/problems/merge-k-sorted-lists/discuss/139673/Sample-javascript-solution
 */
var mergeKLists = function(lists) {
	if (lists.length === 0) {
		return [];
	}

	let result = lists.pop();
	while(lists.length > 0) {
		result = mergeList(result, lists.pop());
	};
	return result;
};

function mergeList(left, right) {
	if (left === null) return right;
	if (right === null) return left;
	while (left !== null && right !== null) {
		if (left.val > right.val) {
			right.next = mergeList(right.next, left);
			return right;
		} else {
			left.next = mergeList(left.next, right);
			return left;
		}
	}
}
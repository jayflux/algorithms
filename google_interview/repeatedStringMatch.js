const { performance } = require('perf_hooks');
/**
 * @param {string} A
 * @param {string} B
 * @return {number}
 * https://leetcode.com/explore/interview/card/google/67/sql-2/469/
 */
var repeatedStringMatch = function(A, B) {
	let count = 1,
		mule = A;

	while (mule.length <= B.length) {
		mule += A;
		count++;
	}
	if (mule.includes(B)) return count;
	mule += A;
	count ++;
	if (mule.includes(B)) return count;

	return -1;
};

let now = performance.now();
let lol = repeatedStringMatch("abcd", "cdabcdab");
console.log(performance.now() - now);
console.log(lol);
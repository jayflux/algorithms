function nextClosestTime(time) {
	// we need to represent time as minites
	// "19:34" -> "19:39"
	let start = parseInt(time.substring(0, 2) * 60) + parseInt(time.substring(3, 5));
	let ans = start;
	let elapsed = 24 * 60;
	// We use Set because its unique
	let allowed = new Set();
	for (let i of time) {
		if (i !== ':') allowed.add(parseInt(i))
	}

	// We will loop through the hours and check if any of them are less than 24 hours, if so we move on
	for (h1 of allowed) for (h2 of allowed) if (h1 * 10 + h2 < 24) {
		// We will loop through the minutes, we only care about combinations less than 60 minutes
		for (m1 of allowed) for (m2 of allowed) if (m1 * 10 + m2 < 60)  {
			let cur = 60 * (h1 * 10 + h2) + (m1 * 10 + m2);
			// There is no Math.floormod in JS, so we will need to implement it manually
			// https://stackoverflow.com/questions/4467539/javascript-modulo-gives-a-negative-result-for-negative-numbers
			let candElapsed = Math.floor(((cur - start) % (24 * 60)) + (24 * 60)) % (24 * 60);
			if (0 < candElapsed && candElapsed < elapsed) {
				ans = cur;
				elapsed = candElapsed;
			}
		}
	}
	let hours = Math.floor(ans / 60).toString().padStart(2, 0);
	let mins = Math.floor(ans % 60).toString().padStart(2, 0);
	return `${hours}:${mins}`
}

console.log(nextClosestTime("23:59"))

// https://www.youtube.com/watch?v=BKOgB5pa57c&t=66s
// This solutions will probably be Big O(n log n)
// For every element in the flowers array we have to do a binary search which is log n
// At the very best we have to do Big O of n, because at the least we have to iterate through the flowers array

// What if we convert the flowers we have i as the position and x as days, so we have a very visual representation

/**
 * @param {number[]} flowers
 * @param {number} k
 * @return {number}
 */
var kEmptySlots = function(flowers, k) {
  let positions = [];
  // The size of the array will be equal to the size of flowers array
  for (let i = 0; i < flowers.length; i++) {
    // the positions of the flowers is not 0 based, so we want them to be 0 based
    // the days are i + 1, because the expected output is expected to be 1 based
    positions[flowers[i] - 1] = i + 1;
  }
  // So since this is a visual representation of the slots, we need to create a sub array who's length is k
  // We need to set up the boundries of the sub array
  // Why is k + 1, because we want the right to be 1 more than left + k
  // [1,0,0,0,0,0,2]
  //  l - - - - - r
  let left = 0, right = k + 1, result = Infinity;
  for (let i = 0; right < positions.length; i++) {
    // so if the day at the current index is larger than the boundries, then thats what we want.
    // because that means before this day has come, the slots in between the boundries are empty
    if(positions[i] > positions[left] && positions[i] > positions[right]) {
      continue;
    }

    // we've reached the right boundry of our sub array
    if (i === right) {
      // We may have multiple days that meets the k slots requirement. Math.min makes sure we get the earliest day.
      // (Math.max) In this case we want the bigger day, we will only have the 2 blooming flowers on the bigger day
      result = Math.min(result, Math.max(positions[left], positions[right]));
    }

    left = i;
    right = i + k + 1;
  }

  return result == Infinity ? -1 : result;
};

console.log(kEmptySlots([1,3,2], 1))
function throttle(fn, duration) {
	let timer = null,
		eventObj = null;

	return function(e) {
		eventObj = e;
		if (timer !== null) {
			timer = setTimeout(function() {
				fn(eventObj);
				timer = null;
			}, duration);
		}
	} 
}


// With arguments
function throttle(fn, duration) {
	let timer = null,
		eventObj = null;

	return function() {
		args = Array.prototype.slice.call(arguments);
		if (timer !== null) {
			timer = setTimeout(function() {
				fn.apply(null, arguments);
				timer = null;
			}, duration);
		}
	} 
}